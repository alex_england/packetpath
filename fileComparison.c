#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int FileCmp(char* File1, char* File2, int nBufferSize){
    float start, end;
    char diff1[16];
    char diff2[16];
    int numBlocks = 0;
    start = clock();
    FILE *f1 = fopen(File1, "rb"); 
    FILE *f2 = fopen(File2, "rb");
    int diffIndex = 0;
    int blockOffset = 0;
    if (f1 == NULL || f2 == NULL) return -1;
    char* lpBuffer1 = (char *)malloc(sizeof(char)*nBufferSize);
    char* lpBuffer2 = (char *)malloc(sizeof(char)*nBufferSize);
    while(1)
    {   
        unsigned int uRead1 = fread(lpBuffer1, sizeof(char), nBufferSize, f1);
        unsigned int uRead2 = fread(lpBuffer2, sizeof(char), nBufferSize, f2);
        //keep 16 bytes of difference
        if(diffIndex < 16){
            for(unsigned int i = 0; i < uRead1 && diffIndex < 16; i++)
            {
                if ((lpBuffer1[i] != lpBuffer2[i])){
                    diff1[diffIndex] = lpBuffer1[i]; 
                    diff2[diffIndex] = lpBuffer2[i];
                    blockOffset = i; //keep track of number of blocks
                    diffIndex++;
                }
            }
        }
        else
        {
            break;
        }
        if ((feof(f1) != 0) && (feof(f2) != 0)){
            break; // both files have nothing more to read and are identical
        }
        numBlocks = numBlocks+1; //next buffer block
    }
    end = clock();
    //free mem and close
    free(lpBuffer1);
    free(lpBuffer2);
    fclose(f1);
    fclose(f2);
    if(blockOffset == 0){ //no difference
        printf("Files are identical");
        printf("\n");
    }
    else
    { //files are different print details
        printf("Number of 8 KB Blocks: %i \n", numBlocks); 
        printf("Offset: %i Bytes \n", blockOffset-16+(8*1024*(numBlocks-1)));
         printf("File1 Difference: ");
        for(int l = 0; l<16; l++){
            printf("%x ", diff1[l]);
        
        }
        printf("\n");   
        printf("File2 Difference: ");
        for(int m = 0; m<16; m++){
            printf("%x ", diff2[m]);
        }
        printf("\n");
    }
    float timeElapsed = ((end - start)/CLOCKS_PER_SEC);
    printf("Time Elapsed: %f Seconds \n", timeElapsed);
    return 0;
}

int main(int argc, char* argv[])
{
    if(argc == 3) //verify 2 files
    {

        FileCmp(argv[1], argv[2], 8*1024); //8kb
    }
    else
    {
        printf("Incorrect number of arguements, please provide 2 files after the program name");
    }
}

